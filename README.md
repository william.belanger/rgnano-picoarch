# RG Nano PicoArch Installer
## Sets PicoArch as the default emulator
**This script is meant to be used on a fresh install of DrUm78's FunKey-OS fork.**
It replaces the stock emulators with their Libretro equivalent and then adds a new frontend entry for Pico-8. It will set up PicoArch configuration so that the "ideal" aspect ratio and screen rotation settings are set by default, namely `Aspect` for Pico-8, Game Gear, Neo Geo Pocket, Gameboy, and Lynx, `Full` for NES, SNES, PSX, GBA, SMS, MD, and PCE, and `Native` for WonderSwan.

## Requirements
This installer requires a Linux OS with Python >= 3.9 and the modules `psutil` and `pyparted`. **Without a Linux computer, it is easily run from a live ISO.**

### Steps to Install from a Linux Live ISO
1. Download a Linux image that ships with a recent Python version (suggested: [Debian 12](https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.1.0-amd64-lxqt.iso)).
2. Write that image to a USB drive using the same tool used for the FunKey OS.
3. Reboot the computer with the USB drive plugged in and launch the Linux live system.
4. (Debian) Open a terminal and setup a network connection with `cmst`.
5. Proceed with the instructions in the `Installation` section.

## Installation
1. Install the latest [DrUm78's FunKey OS](https://github.com/DrUm78/FunKey-OS/releases) fork on a microSD card.
2. Place the card in the RG Nano and proceed to the first boot.
3. Shutdown the device and then mount the card on a Linux computer.
4. Download the content of this repository and open the folder using a file manager.
5. (Debian) Right click in the folder and choose `Open in a terminal`.
6. Install the dependencies and execute the script with root privileges:
```
# For Arch Linux:
sudo pacman -Syu python-psutil python-pyparted
sudo python picoarch_installer_rgnano.py

# For Debian Linux:
sudo apt-get update
sudo apt-get install python3-psutil python3-parted
sudo python3 picoarch_installer_rgnano.py
```

## What then ?
1. You may update the RetroFE platforms list in `/rootfs/usr/games/collections/Main/menu.txt` to select which platform you would like to use.
2. Add your BIOS files to `/mnt/FunKey/.picoarch/system`.
3. Add your ROMs to `/mnt/`.

## Note
The cores were compiled by neonloop (02/02/2023). The code license is MIT.
