#!/usr/bin/python3
import hashlib
import os
import parted
import psutil
import stat
import subprocess
import zipfile
from collections import namedtuple
from pathlib import Path

Archive = namedtuple("Archive", ["zip", "md5"])
Partition = namedtuple("Partition", ["path", "filesystem", "flags", "size"])
cwd = Path(__file__).parent

picoarch_cfg = Archive(cwd / "picoarch_cfg.zip", "52406157a33d1f81c5dfed14a834d83c")
cores = Archive(cwd / "cores_neonloop.zip", "2723f70f7a9c97bed7cc965c3426f859")
theme = Archive(cwd / "theme_pico8.zip", "194ba414c73b4fbc604089e16934347f")
cores_id = {
    "gameboy": "gambatte_libretro.so",
    "gamegear": "smsplus-gx_libretro.so",
    "gba": "gpsp_libretro.so",
    "lynx": "handy_libretro.so",
    "megadrive": "picodrive_libretro.so",
    "NES": "fceumm_libretro.so",
    "ngp": "mednafen_ngp_libretro.so",
    "pce": "beetle-pce-fast_libretro.so",
    "pico-8": "fake-08_libretro.so",
    "psone": "pcsx_rearmed_libretro.so",
    "sms": "smsplus-gx_libretro.so",
    "snes": "snes9x2010_libretro.so",
    "wonderswan": "mednafen_wswan_libretro.so",
}


class DriveAnalyzer:
    DeviceInfo = namedtuple("DeviceInfo", ["dev", "path"])

    @classmethod
    def find_sdcard(cls):
        candidates = {}
        drives = cls._get_drives_info()
        for drive, partitions in drives.items():
            if cls._is_candidate(drive, partitions):
                rootfs = partitions[drive + "2"].path
                rootfs = Path(rootfs) if rootfs else None
                mnt = partitions[drive + "4"].path
                mnt = Path(mnt) if mnt else None
                candidates[drive] = {
                    "rootfs": cls.DeviceInfo(drive + "2", rootfs),
                    "mnt": cls.DeviceInfo(drive + "4", mnt)
                }
        if len(candidates) == 1:
            return next(iter(candidates.values()))
        return None

    def _is_candidate(drive, partitions):
        return len(partitions) == 4 and (
            partitions[drive + "1"].size == 100.0
            and partitions[drive + "2"].size == 1024.0
            and partitions[drive + "3"].size == 128.0
            and partitions[drive + "1"].filesystem == "ext4"
            and partitions[drive + "2"].filesystem == "ext4"
            and partitions[drive + "3"].filesystem == "linux-swap(v1)"
            and partitions[drive + "4"].filesystem == "fat32"
            and partitions[drive + "1"].flags == "000000000000000000000"
            and partitions[drive + "2"].flags == "100000000000000000000"
            and partitions[drive + "3"].flags == "001000000000000000000"
            and partitions[drive + "4"].flags == "000000100000000000000"
        )

    def _get_block_devices():
        block_devices = []
        for device_path in Path("/sys/class/block").iterdir():
            if device_path.exists():
                block_devices.append("/dev/" + device_path.name)
        return [dev for dev in block_devices if len(dev) == 8]  # len("/dev/sd*") == 8

    @classmethod
    def _get_drives_info(cls):
        drives = {}
        block_devices = cls._get_block_devices()
        for device_path in block_devices:
            try:
                device = parted.getDevice(device_path)
                disk = parted.newDisk(device)
                drive_properties = {}
                for partition in disk.partitions:
                    drive_properties[partition.path] = cls._get_partition_info(partition)
                drives[device_path] = drive_properties
            except Exception:
                pass
        return drives

    def _get_partition_flags(partition):
        flags = []
        for flag in range(1, 22):
            flag = int(partition.getFlag(flag))
            flags.append(str(flag))
        return "".join(flags)

    @classmethod
    def _get_partition_info(cls, partition):
        try:
            dev = partition.path
            partitions = psutil.disk_partitions()
            path = next((p.mountpoint for p in partitions if p.device == dev), None)
            fs = partition.fileSystem.type
            flags = cls._get_partition_flags(partition)
            size = partition.getSize()
            return Partition(path, fs, flags, size)
        except Exception:
            return Partition(None, None, None, None)


class DriveManager:
    @staticmethod
    def mount(device, path):
        Path(path).mkdir(parents=True, exist_ok=True)
        command = ["sudo", "mount", device, str(path)]
        print(">", " ".join(command))
        subprocess.run(command, check=True)

    @staticmethod
    def unmount(path):
        if path:
            command = ["sudo", "umount", str(path)]
            print(">", " ".join(command))
            subprocess.run(command, check=True)


class Validator:
    @staticmethod
    def prompt(question: str, default=None) -> bool:
        choices = ("", "y", "n") if default in ("yes", "no") else ("y", "n")
        hint = "Y/n" if default == "yes" else "y/n"
        hint = "y/N" if default == "no" else hint
        reply = None

        while reply not in choices:
            reply = input(f"{question} ({hint}): ").lower()
        return (reply == "y") if default != "yes" else (reply in ("", "y"))

    @classmethod
    def tests(cls, sdcard: object) -> bool:
        if not cores.zip.exists():
            print(f"Archive not found '{cores.zip}'")
            return False

        elif not theme.zip.exists():
            print(f"Archive not found '{theme.zip}'")
            return False

        elif not picoarch_cfg.zip.exists():
            print(f"Archive not found '{picoarch_cfg.zip}'")
            return False

        elif cls._md5sum(cores.zip) != cores.md5:
            print(f"Bad MD5 checksum for '{cores.zip}'")
            return False

        elif cls._md5sum(theme.zip) != theme.md5:
            print(f"Bad MD5 checksum for '{theme.zip}'")
            return False

        elif cls._md5sum(picoarch_cfg.zip) != picoarch_cfg.md5:
            print(f"Bad MD5 checksum for '{picoarch_cfg.zip}'")
            return False

        elif not sdcard:
            print("Could not find RG Nano SD Card")
            return False

        print("This script is meant to be used on a fresh install of DrUm78's FunKey-OS fork. It replaces")
        print("the stock emulators with their Libretro equivalent, then adds a frontend entry for Pico-8.")
        print(f"\n\033[1mFiles in {sdcard['rootfs'].dev[:-1]} will be overwritten !\033[0m\n")
        if not cls.prompt("Proceed with installation ?"):
            print("Canceled by user")
            return False
        return True

    @staticmethod
    def _md5sum(file_path):
        md5_hash = hashlib.md5()
        with open(file_path, "rb") as file:
            data = file.read(4096)  # Read the file in chunks to preserve memory
            while data:
                md5_hash.update(data)
                data = file.read(4096)
        return md5_hash.hexdigest()


class Installer:
    @staticmethod
    def configuration(path: Path):
        print(f"Installing PicoArch configuration: '{path}'")
        path.mkdir(parents=True, exist_ok=True)
        with zipfile.ZipFile(picoarch_cfg.zip, "r") as f:
            f.extractall(path)

    @staticmethod
    def cores(path: Path):
        print(f"Installing Libretro cores: '{path}'")
        path.mkdir(parents=True, exist_ok=True)
        with zipfile.ZipFile(cores.zip, "r") as f:
            f.extractall(path)

    @staticmethod
    def delete_roms(roms_dir: Path):
        dirs = [
            roms_dir / "Atari lynx",
            roms_dir / "Game Boy",
            roms_dir / "Game Boy Advance",
            roms_dir / "Game Boy Color",
            roms_dir / "Game Gear",
            roms_dir / "Neo Geo Pocket",
            roms_dir / "NES",
            roms_dir / "PCE-TurboGrafx",
            roms_dir / "Pico-8",
            roms_dir / "PS1",
            roms_dir / "Sega Genesis",
            roms_dir / "Sega Master System",
            roms_dir / "SNES",
            roms_dir / "WonderSwan"
        ]
        for d in dirs:
            for f in d.glob("*"):
                if f.is_file():
                    f.unlink()
                    print(f"Deleted '{f.parent}/{f.name}'")

    @staticmethod
    def launchers(path: Path):
        print(f"Updating launchers: '{path}'")
        for core, filename in cores_id.items():

            with open(path / f"{core}.conf", "w", encoding="utf-8") as f:
                f.write(f"executable = %RETROFE_PATH%/launchers/{core}_launch_picoarch.sh\n")
                f.write('arguments = "%ITEM_FILEPATH%"')

            core_path = Path("/usr/games/cores") / filename
            launcher_path = path / f"{core}_launch_picoarch.sh"
            with open(launcher_path, "w", encoding="utf-8") as f:
                f.write("#!/bin/sh\n")
                f.write(f'picoarch {core_path} "$1" &\n')
                f.write("pid record $!\n")
                f.write("wait $!\n")
                f.write("pid erase")

            permissions = os.stat(launcher_path).st_mode
            os.chmod(launcher_path, (permissions | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH))

    @staticmethod
    def pico8(path: Path):
        def has_menu_entry(menu: Path) -> bool:
            with open(menu, "r") as file:
                for line in file:
                    if line.count("Pico-8"):
                        return True
                return False

        layout_dir = path / "layouts/RetroRoomCovers/collections/Pico-8/system_artwork"
        print(f"Installing RetroFE theme entry for Pico-8: '{layout_dir}'")
        layout_dir.mkdir(parents=True, exist_ok=True)
        with zipfile.ZipFile(theme.zip, "r") as f:
            f.extract("bg.png", layout_dir)
            f.extract("device_W240.png", layout_dir)

        collection_dir = path / "collections/Pico-8"
        (collection_dir / "system_artwork").mkdir(parents=True, exist_ok=True)
        with zipfile.ZipFile(theme.zip, "r") as f:
            f.extract("settings.conf", collection_dir)
            f.extract("fallback.png", collection_dir / "system_artwork")

        menu_file = path / "collections/Main/menu.txt"
        print(f"Updating platforms menu: '{menu_file}'")
        if not has_menu_entry(menu_file):
            with open(menu_file, "a") as f:
                f.write("\nPico-8")


def main():
    sdcard = DriveAnalyzer.find_sdcard()
    if Validator.tests(sdcard):
        print("\nMounting partitions to a known location")
        DriveManager.unmount(sdcard["mnt"].path)
        DriveManager.unmount(sdcard["rootfs"].path)
        DriveManager.mount(sdcard["mnt"].dev, "/mnt/rgnano-user")
        DriveManager.mount(sdcard["rootfs"].dev, "/mnt/rgnano-root")

        Installer.cores(Path("/mnt/rgnano-root/usr/games/cores"))
        Installer.launchers(Path("/mnt/rgnano-root/usr/games/launchers"))
        Installer.configuration(Path("/mnt/rgnano-user/FunKey"))

        if Validator.prompt("\nWould you like to install Pico-8 ?", default="yes"):
            Installer.pico8(Path("/mnt/rgnano-root/usr/games"))

        if Validator.prompt("\nWould you like to remove all ROMs in /mnt/rgnano-user ?", default="no"):
            if Validator.prompt("Are you sure ?", default="no"):
                Installer.delete_roms(Path("/mnt/rgnano-user"))

        print("\nUnmounting partitions")
        DriveManager.unmount("/mnt/rgnano-user")
        DriveManager.unmount("/mnt/rgnano-root")
        print("\nInstallation completed")
    else:
        print("\nInstallation aborted")
    input("Press ENTER to exit...")


if __name__ == "__main__":
    if os.geteuid():
        print("Root privileges required")
    else:
        main()
